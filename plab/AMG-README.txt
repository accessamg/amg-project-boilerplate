Starting Pattern Lab:

In the project ‘plab’ directory (next to this README), double-click "Start Pattern Lab.app" to start Pattern Lab. You will see a Terminal window appear as it starts. Keep this window open as you work.

Once started, you can navigate to http://localhost:8080 in a web browser to interactively view the current pattern library. If the current library is empty, there will be nothing to see. By default, Pattern Lab shuts down after a while. To start it again, simply double-click on "Start Pattern Lab.app". We may adjust or increase this shut down time in the future.